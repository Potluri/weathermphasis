//
//  Weather.swift
//  WeatherApp
//
//  Created by Potluri on 5/16/23.
//

import Foundation

struct WeatherData: Codable {
    var coord: Coordinates?
    var weather: [Weather]?
    var main: MainWeather?
    var visibility: Double?
    var wind: Wind?
    var rain: Rain?
    var clouds: Clouds?
    var sys: SysWeather?
    var name: String?
}

struct Coordinates: Codable {
    var lon: Double?
    var lat: Double?
}

struct Weather: Codable {
    var id: Int?
    var main: String?
    var description: String?
    var icon: String?
}

struct MainWeather: Codable {
    var temp: Double?
    var temp_min: Double?
    var temp_max: Double?
    var pressure: Double?
    var humidity: Double?
    var sea_level: Double?
    var grnd_level: Double?
}

struct Wind: Codable {
    var speed: Double?
    var deg: Int?
    var gust: Double?
}

struct Rain: Codable {
    var lh: Double?
}

struct Clouds: Codable {
    var all: Int
}

struct SysWeather: Codable {
    var country: String?
    var sunrise: Int
    var sunset: Int
}

struct StorageData: Identifiable, Comparable, Codable {
    var id = UUID()
    let city: String
    var searchCount: Int = 0
    
    static func < (lhs: Self, rhs: Self) -> Bool {
        lhs.searchCount < rhs.searchCount
    }
}
