//
//  ContentView.swift
//  WeatherApp
//
//  Created by Potluri on 4/21/23.
//

import SwiftUI
import CoreLocationUI

struct ContentView: View {
    @State private var searchString = ""
    @StateObject var viewModel = ViewModel()
    var body: some View {
        NavigationStack {
            ZStack {
                if viewModel.backgroundCallStatus == .failed {
                    Text("Please check the entered ZipCode/CityName")
                } else if viewModel.locationError == .unauthorized && viewModel.weatherData == nil {
                    VStack {
                        Text ("Unable to determine location. Please provide location access by going into settings or search by using city name or ZipCode.")
                            .padding()
                        Button {
                            UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
                        } label: {
                            Text("Open Settings")
                        }
                    }
                } else {
                    VStack {
                        RecentSearchView(viewModel: viewModel)
                            .frame(height: 50)
                        Spacer()
                            .frame(height: 8)
                        TableViewWeather(weatherData: viewModel.weatherData)
                    }
                }
                if viewModel.backgroundCallStatus == .fetching {
                    ProgressView {
                        Text("Loading...")
                    }
                }
            }
            .navigationTitle(Text("Weather App"))
            .navigationBarTitleDisplayMode(.inline)
        }
        .searchable(text: $searchString, prompt: "City/Zipcode", suggestions: {
            if let data = viewModel.userDefaultsData {
                ForEach (data) { suggestion in
                    Text(suggestion.city)
                        .searchCompletion(suggestion.city)
                }
            }
        })
        .autocorrectionDisabled()
        .onSubmit(of: .search, {
            viewModel.loadDataFor(searchString: searchString)
        })
        .onAppear {
            viewModel.loadWeatherDataOnAppear()
        }
    }
}

struct Row: View {
    let title: String
    let text: String
    var body: some View {
        HStack {
            Text(title)
            Spacer()
            Text(text)
        }
    }
}

struct TableViewWeather: View {
    let weatherData: WeatherData?
    var body: some View {
        List {
            Section(header: Text(weatherData?.name ?? "USA")) {
                Row(title: "Weather", text: weatherData?.weather?.first?.main ?? "")
                Row(title: "Description", text: weatherData?.weather?.first?.description ?? "")
                Row(title: "Latitude", text: String(weatherData?.coord?.lat ?? 0))
                Row(title: "Longitude", text: String(weatherData?.coord?.lon ?? 0))
                Row(title: "Sunrise", text: String(weatherData?.sys?.sunrise ?? 0))
                Row(title: "Sunset", text: String(weatherData?.sys?.sunset ?? 0))
            }
            .headerProminence(.increased)
            Section(header: Text("Main")) {
                Row(title: "Temperature", text: String(weatherData?.main?.temp ?? 0))
                Row(title: "Max Temperature", text: String(weatherData?.main?.temp_max ?? 0))
                Row(title: "Min Temperature", text: String(weatherData?.main?.temp_min ?? 0))
                Row(title: "Pressure", text: String(weatherData?.main?.pressure ?? 0))
                Row(title: "Humidity", text: String(weatherData?.main?.humidity ?? 0))
                Row(title: "Sea Level", text: String(weatherData?.main?.sea_level ?? 0))
                Row(title: "Ground Level", text: String(weatherData?.main?.grnd_level ?? 0))
            }
            Section(header: Text("Rain")) {
                Row(title: "lh", text: String(weatherData?.rain?.lh ?? 0))
            }
            Section(header: Text("Wind")) {
                Row(title: "Speed", text: String(weatherData?.wind?.speed ?? 0))
                Row(title: "Deg", text: String(weatherData?.wind?.deg ?? 0))
                Row(title: "Gust", text: String(weatherData?.wind?.gust ?? 0))
            }
            Section(header: Text("Visibility")) {
                Row(title: "Visibility", text: String(weatherData?.visibility ?? 0))
            }
        }
        .listStyle(.grouped)
    }
}

struct RecentSearchView: View {
    @ObservedObject var viewModel: ViewModel
    var body: some View {
        if let userData = viewModel.userDefaultsData {
            ScrollView(.horizontal) {
                LazyHStack {
                    ForEach(userData) { array in
                        Text(array.city)
                            .padding()
                            .background(Color(UIColor.systemGroupedBackground))
                            .cornerRadius(8)
                            .onTapGesture {
                                viewModel.loadDataFor(searchString: array.city)
                            }
                    }
                }
                .padding()
            }
            .background(.white)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
