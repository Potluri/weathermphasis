//
//  WeatherAppApp.swift
//  WeatherApp
//
//  Created by Potluri on 4/21/23.
//

import SwiftUI

@main
struct WeatherAppApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
