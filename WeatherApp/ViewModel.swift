//
//  ViewModel.swift
//  WeatherApp
//
//  Created by Potluri on 5/16/23.
//

import Foundation
import Combine
import CoreLocation

enum LocationError: Error {
    case unauthorized
    case unableToDetermineLocation
}

enum BackgroundURLFetchStatus: Equatable {
    case idle
    case fetching
    case failed
}

class ViewModel: NSObject, ObservableObject {
    private let manager = CLLocationManager()
    @Published private(set) var weatherData: WeatherData?
    private var location: CLLocation? {
        didSet {
            loadDataFor(location: location)
        }
    }
    @Published private(set) var locationError: LocationError?
    @Published private(set) var backgroundCallStatus: BackgroundURLFetchStatus = .idle
    private var cancellable: AnyCancellable?
    @Published private(set) var userDefaultsData: [StorageData]?
    
    override init() {
        super.init()
        manager.delegate = self
        loadDataFromUserDefaults()
        requestLocation()
    }
    
    private func makeURLCallfor(url: URL) {
        cancellable?.cancel()
        backgroundCallStatus = .fetching
        let publisher = URLSession.shared.dataTaskPublisher(for: url)
            .map { (data, urlresponse) in
                try? JSONDecoder().decode(WeatherData.self, from: data)
            }
            .receive(on: DispatchQueue.main)
        cancellable = publisher.sink(receiveCompletion: { result in
            switch result {
            case .finished:
                print("sucess")
            case .failure:
                print("failed")
            }
        }, receiveValue: { [weak self] weather in
            self?.weatherData = weather ?? WeatherData()
            self?.backgroundCallStatus = weather?.coord?.lat != nil ? .idle : .failed
            if self?.backgroundCallStatus == .idle {
                self?.saveCityToUserDefaults(city: weather?.name ?? "")
            }
        })
    }
    
    func loadWeatherDataOnAppear() {
        if let location = location {
            loadDataFor(location: location)
        } else if let city = userDefaultsData?.first?.city {
            loadDataFor(searchString: city)
        } else {
            backgroundCallStatus = .failed
        }
    }
    
    func loadDataFor(location: CLLocation?) {
        guard let location = location, let url = URL(string: "https://api.openweathermap.org/data/2.5/weather?lat=\(location.coordinate.latitude)&lon=\(location.coordinate.longitude)&appid=f4d52541a2c6d292085aaa3ee0b53d56") else { return }
        makeURLCallfor(url: url)
    }
    
    func loadDataFor(searchString: String) {
        var url: URL?
        if isZipCode(zip: searchString) {
            url = URL(string: "https://api.openweathermap.org/data/2.5/weather?zip=\(searchString),us&appid=f4d52541a2c6d292085aaa3ee0b53d56")
        } else if isValidCity(city: searchString) {
            url = URL(string: "https://api.openweathermap.org/data/2.5/weather?q=\(searchString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "")&appid=f4d52541a2c6d292085aaa3ee0b53d56")
        }
        guard let url = url else { backgroundCallStatus = .failed; return }
        makeURLCallfor(url: url)
    }
    
    func requestLocation() {
        if manager.authorizationStatus == .authorizedAlways ||  manager.authorizationStatus == .authorizedWhenInUse {
            manager.requestLocation()
        } else {
            manager.requestWhenInUseAuthorization()
        }
    }
}

//ToDo:- Ideally I would have the LocationManager as a singleton in a separate class with having Combine's Futures as callbacks
extension ViewModel: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        location = locations.last
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        if let error = error as? CLError, error.code == .denied {
            locationError = .unauthorized
        } else {
            locationError = .unableToDetermineLocation
        }
    }
    
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        switch manager.authorizationStatus {
        case .authorizedAlways, .authorizedWhenInUse:
            manager.requestLocation()
        case .denied, .notDetermined, .restricted:
            locationError = .unauthorized
        @unknown default:
            locationError = .unauthorized
        }
    }
}

extension ViewModel {
    ///checks if the entered zipcode matches US zipcode standards to avoid unnecessary URL call
    private func isZipCode(zip: String) -> Bool {
        return NSPredicate(format: "SELF MATCHES %@", "^\\d{5}(?:[-\\s]?\\d{4})?$").evaluate(with: zip)
    }
    ///Checks if the city name entered has any unwanted chars and avoids an unwanted URL call
    private func isValidCity(city: String) -> Bool {
        let characterset = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ ")
        return city.rangeOfCharacter(from: characterset.inverted) == nil
    }
    ///Retrieves the data from UserDefaults
    private func loadDataFromUserDefaults() {
        guard let data = UserDefaults.standard.object(forKey: "storageInUserDefaults") as? Data, let storageData = try? JSONDecoder().decode([StorageData].self, from: data) else { return }
        self.userDefaultsData = storageData
    }
    ///Checks for the city in existing stored data and stores on how frequently the data for city is accessed (to prioritize over other cities)
    private func saveCityToUserDefaults(city: String) {
        guard !city.isEmpty else { return }
        guard var userDefaultsData = userDefaultsData else { setUserDefaults(value: [StorageData(city: city)]); return }
        let count = userDefaultsData.count
        var valueFound = false
        for index in 0..<count {
            var data = userDefaultsData[index]
            if data.city == city {
                data.searchCount += 1
                valueFound = true
                userDefaultsData[index] = data
                userDefaultsData = userDefaultsData.sorted(by: >)
                break
            }
        }
        if !valueFound {
            userDefaultsData.append(StorageData(city: city))
        }
        setUserDefaults(value: userDefaultsData)
    }
    ///Saves data to UserDefaults
    private func setUserDefaults(value: [StorageData]) {
        guard let encodedData = try? JSONEncoder().encode(value) else { return }
        UserDefaults.standard.set(encodedData, forKey: "storageInUserDefaults")
        userDefaultsData = value
    }
}
